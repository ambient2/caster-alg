# caster-alg

Linear algebra library for ray caster

## Examples

### Camera One (camera_one.rs)

```bash
$ cargo run --example camera_one
# Cropped...
```

![single_green_sphere](./examples/camera_one.rs.png)

### Composite Object (composite_object_one_camera.rs)

```bash
$ cargo run --example composite_object_one_camera
# Cropped...
```

![two_green_spheres](./examples/composite_object_one_camera.rs.png)
