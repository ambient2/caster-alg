/// `Point`s are N-dimensional tuples
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Point<T, const N: usize> {
    components: [T; N],
}

impl<T, const N: usize> Point<T, N> {
    /// Constructs a `Point` from an N-dimensional array
    pub fn from_components(components: [T; N]) -> Self {
        Point { components }
    }

    /// Returns the raw components comprising the `Point`
    pub fn components(&self) -> &[T; N] {
        &self.components
    }
}

impl<T, const N: usize> Point<T, N>
where
    T: Default + Clone + Sized,
{
    pub fn dyad_compose<F>(&self, rhs: &Self, f: F) -> Self
    where
        F: Fn(&T, &T) -> T,
    {
        let mut nxt = self.components().clone();

        self.components()
            .iter()
            .zip(rhs.components())
            .enumerate()
            .for_each(|(ix, (lc, rc))| {
                nxt[ix] = f(lc, rc);
            });
        Point::from_components(nxt)
    }

    pub fn unary_compose<F>(&self, f: F) -> Self
    where
        F: Fn(&T) -> T,
    {
        let mut nxt = self.components().clone();
        self.components().iter().enumerate().for_each(|(ix, el)| {
            nxt[ix] = f(el);
        });
        Point::from_components(nxt)
    }
}

impl<T, const N: usize> Default for Point<T, N>
where
    T: Default + Clone + Copy,
{
    fn default() -> Self {
        Self {
            components: [Default::default(); N],
        }
    }
}

impl<T, const N: usize> std::ops::Add for Point<T, N>
where
    T: std::ops::Add<Output = T> + Default + Clone,
{
    type Output = Point<T, N>;

    fn add(self, rhs: Self) -> Point<T, N> {
        Point::dyad_compose(&self, &rhs, |lc, rc| lc.clone() + rc.clone())
    }
}

impl<T, const N: usize> std::ops::Sub for Point<T, N>
where
    T: std::ops::Sub<Output = T> + Default + Clone,
{
    type Output = Point<T, N>;

    fn sub(self, rhs: Self) -> Self::Output {
        self.dyad_compose(&rhs, |lc, rc| lc.clone() - rc.clone())
    }
}

impl<T, const N: usize> std::ops::Neg for Point<T, N>
where
    T: std::ops::Neg<Output = T> + Default + Clone,
{
    type Output = Point<T, N>;

    fn neg(self) -> Self::Output {
        self.unary_compose(|el| -el.clone())
    }
}

impl<T, const N: usize> std::ops::Mul for Point<T, N>
where
    T: std::ops::Mul<Output = T> + Default + Clone,
{
    type Output = Point<T, N>;
    fn mul(self, rhs: Self) -> Self::Output {
        self.dyad_compose(&rhs, |el1, el2| el1.clone() * el2.clone())
    }
}

impl<T, const N: usize> std::ops::Div for Point<T, N>
where
    T: std::ops::Div<Output = T> + Default + Clone,
{
    type Output = Point<T, N>;
    fn div(self, rhs: Self) -> Self::Output {
        self.dyad_compose(&rhs, |el1, el2| el1.clone() / el2.clone())
    }
}

impl<T, const N: usize> std::fmt::Display for Point<T, N>
where
    T: std::fmt::Display,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let joined: Vec<String> = self.components().iter().map(|e| format!("{}", e)).collect();
        let mut comps = String::new();
        comps.push_str(&joined[0]);
        for x in joined.iter().skip(1) {
            let nxt = format!(",{}", x);
            comps.push_str(&nxt);
        }
        write!(f, "({})", comps)
    }
}

#[cfg(test)]
mod test {
    use super::Point;

    #[test]
    fn points_add_componentwise() {
        let cs = [1.0 as f32, 2.0, 3.0, 4.0];
        let p = Point::from_components(cs) + Point::from_components(cs);

        assert_eq!(&[2.0 as f32, 4.0, 6.0, 8.0], p.components())
    }

    #[test]
    fn points_sub_componentwise() {
        let cs = [1.0 as f32, 2.0, 3.0, 4.0];
        let p = Point::from_components(cs) - Point::from_components([1.0; 4]);

        assert_eq!(&[0.0 as f32, 1.0, 2.0, 3.0], p.components())
    }

    #[test]
    fn points_mul_componentwise() {
        let cs = [1.0 as f32, 2.0, 3.0, 4.0];
        let p = Point::from_components(cs) * Point::from_components(cs);

        assert_eq!(&[1.0 as f32, 4.0, 9.0, 16.0], p.components())
    }

    #[test]
    fn points_div_componentwise() {
        let cs = [1.0 as f32, 4.0, 9.0];
        let p = Point::from_components(cs) / Point::from_components([1.0, 2.0, 3.0]);
        assert_eq!(&[1.0 as f32, 2.0, 3.0], p.components())
    }

    #[test]
    fn points_neg_componentwise() {
        let cs = [1.0, -1.0, 1.0];
        let p = -(Point::from_components(cs));
        assert_eq!(&[-1.0, 1.0, -1.0], p.components())
    }

    #[test]
    fn points_display_as_ntuple() {
        let p = Point::from_components([1.0, 2.0]);
        assert_eq!("(1,2)", format!("{}", p).as_str())
    }
}
