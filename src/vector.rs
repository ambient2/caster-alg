use crate::{Floatlike, Point};

use super::Normalizable;

#[derive(Debug, Clone, PartialEq)]
pub struct AlgVector<T, const N: usize> {
    coords: Point<T, N>,
}

impl<T, const N: usize> AlgVector<T, N>
where
    T: Default + Clone,
{
    pub fn dyad_compose<F>(&self, rhs: &Self, f: F) -> AlgVector<T, N>
    where
        F: Fn(&T, &T) -> T,
    {
        let l = &self.coords;
        let r = &rhs.coords;
        let coords = Point::dyad_compose(l, r, f);
        AlgVector { coords }
    }

    pub fn unary_compose<F>(&self, f: F) -> AlgVector<T, N>
    where
        F: Fn(&T) -> T,
    {
        let l = &self.coords;
        let coords = Point::unary_compose(&l, f);
        AlgVector { coords }
    }
}

impl<T, const N: usize> From<Point<T, N>> for AlgVector<T, N> {
    fn from(p: Point<T, N>) -> Self {
        AlgVector { coords: p }
    }
}

impl<T, const N: usize> std::ops::Add for AlgVector<T, N>
where
    T: Default + Clone + std::ops::Add<Output = T>,
{
    type Output = AlgVector<T, N>;

    fn add(self, rhs: Self) -> Self::Output {
        AlgVector::dyad_compose(&self, &rhs, |l, r| l.clone() + r.clone())
    }
}

impl<T, const N: usize> std::ops::Sub for AlgVector<T, N>
where
    T: Default + Clone + std::ops::Sub<Output = T>,
{
    type Output = AlgVector<T, N>;

    fn sub(self, rhs: Self) -> Self::Output {
        AlgVector::dyad_compose(&self, &rhs, |l, r| l.clone() - r.clone())
    }
}

impl<T, const N: usize> std::ops::Mul for AlgVector<T, N>
where
    T: Default + Clone + std::ops::Mul<Output = T> + std::ops::Add<Output = T>,
{
    type Output = T;

    fn mul(self, rhs: Self) -> Self::Output {
        let lp = self.coords;
        let rp = rhs.coords;

        let np = lp * rp;
        np.components()
            .iter()
            .fold(Default::default(), |curr: T, nxt| curr + nxt.clone())
    }
}

impl<T, const N: usize> Normalizable for AlgVector<T, N>
where
    T: Floatlike,
{
    type Output = Self;
    fn normalize(&self) -> Self {
        let mag = self.clone() * self.clone();
        let sq = mag.psqrt();
        self.unary_compose(|e| {
            if sq.is_some() {
                e.clone() / sq.clone().unwrap()
            } else {
                e.clone()
            }
        })
    }
}

impl<T, const N: usize> std::fmt::Display for AlgVector<T, N>
where
    T: std::fmt::Display,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let joined: Vec<String> = self
            .coords
            .components()
            .iter()
            .map(|e| format!("{}", e))
            .collect();
        let mut comps = String::new();
        comps.push_str(&joined[0]);
        for x in joined.iter().skip(1) {
            let nxt = format!(",{}", x);
            comps.push_str(&nxt);
        }
        write!(f, "<|{}|>", comps)
    }
}

impl<T, const N: usize> AlgVector<T, N> {
    pub fn coords(&self) -> &Point<T, N> {
        &self.coords
    }
}

#[cfg(test)]
mod test {
    use crate::Normalizable;

    use super::{AlgVector, Point};

    #[test]
    fn alg_vector_mul_is_dot_product() {
        let l = AlgVector::from(Point::from_components([1.0, 2.0, 3.0]));
        let r = AlgVector::from(Point::from_components([-2.0, 1.0, 1.0]));
        let res = l * r;
        assert_eq!(3.0, res)
    }

    #[test]
    fn alg_vector_adds_componentwise() {
        let l = AlgVector::from(Point::from_components([1.0, 2.0, 3.0]));
        let r = AlgVector::from(Point::from_components([-2.0, 1.0, 1.0]));
        let res = l + r;
        assert_eq!(
            AlgVector::from(Point::from_components([-1.0, 3.0, 4.0])),
            res
        )
    }

    #[test]
    fn alg_vector_subs_componentwise() {
        let l = AlgVector::from(Point::from_components([1.0, 2.0, 3.0]));
        let r = AlgVector::from(Point::from_components([-2.0, 1.0, 1.0]));
        let res = l - r;
        assert_eq!(
            AlgVector::from(Point::from_components([3.0, 1.0, 2.0])),
            res
        )
    }

    #[test]
    fn alg_vector_norms_to_unit() {
        let p = AlgVector::from(Point::from_components([2.0, 2.0]));
        let res = p.normalize();
        assert_eq!(
            &[(2.0 as f32).sqrt() / 2.0, (2.0 as f32).sqrt() / 2.0],
            res.coords.components()
        )
    }

    #[test]
    fn alg_vector_displays_correctly() {
        let p = AlgVector::from(Point::from_components([-1.2, 2.4]));
        assert_eq!("<|-1.2,2.4|>", format!("{}", p))
    }
}
