use crate::{AlgVector, Point};

#[derive(Debug, Clone, PartialEq)]
pub struct Ray<T, const N: usize> {
    loc: Point<T, N>,
    direction: AlgVector<T, N>,
}

impl<T, const N: usize> Ray<T, N> {
    pub fn loc(&self) -> &Point<T, N> {
        &self.loc
    }

    pub fn direction(&self) -> &AlgVector<T, N> {
        &self.direction
    }

    pub fn new(loc: Point<T, N>, direction: AlgVector<T, N>) -> Ray<T, N> {
        Ray { loc, direction }
    }
}
