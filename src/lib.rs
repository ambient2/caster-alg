//! Generic algebra library to support higher-level optical rendering libraries

mod point;
mod vector;
pub use point::Point;
pub use vector::AlgVector;
mod ray;
mod surface;
pub use ray::Ray;
pub use surface::CompositeSurface;
pub use surface::Plane;
pub use surface::Sphere;
pub use surface::Surface;

pub trait Normalizable {
    type Output;
    fn normalize(&self) -> Self;
}

pub trait PartiallyDefindSquareRoot: Sized {
    fn psqrt(&self) -> Option<Self>;
}

impl PartiallyDefindSquareRoot for f64 {
    fn psqrt(&self) -> Option<Self> {
        if self < &0.0 {
            None
        } else {
            Some(self.sqrt())
        }
    }
}

impl PartiallyDefindSquareRoot for f32 {
    fn psqrt(&self) -> Option<Self> {
        if self < &0.0 {
            None
        } else {
            Some(self.sqrt())
        }
    }
}

pub trait Floatlike:
    std::ops::Add<Output = Self>
    + std::ops::Sub<Output = Self>
    + std::ops::Mul<Output = Self>
    + std::ops::Div<Output = Self>
    + std::ops::Neg<Output = Self>
    + std::cmp::PartialOrd
    + Default
    + Clone
    + Sized
    + PartiallyDefindSquareRoot
{
}

impl Floatlike for f64 {}
impl Floatlike for f32 {}
