use crate::{AlgVector, Point, Ray};

use super::Surface;

#[derive(Debug, Clone, PartialEq)]
pub struct Plane<T, const N: usize> {
    origin: Point<T, N>,
    norm: AlgVector<T, N>,
}

impl<T, const N: usize> Plane<T, N> {
    pub fn new(origin: Point<T, N>, norm: AlgVector<T, N>) -> Plane<T, N> {
        Plane { origin, norm }
    }

    pub fn origin(&self) -> &Point<T, N> {
        &self.origin
    }

    pub fn norm(&self) -> &AlgVector<T, N> {
        &self.norm
    }
}

impl<T, const N: usize> Plane<T, N>
where
    T: Default
        + Copy
        + std::ops::Mul<Output = T>
        + std::ops::Add<Output = T>
        + std::ops::Sub<Output = T>
        + std::ops::Div<Output = T>
        + std::cmp::PartialEq,
{
    pub fn intersection_point(&self, ray: &Ray<T, N>) -> Option<Point<T, N>> {
        let denom = self.norm().clone() * ray.direction().clone();
        if denom.clone() == Default::default() {
            return None;
        } else {
            let orig = self.origin().clone();
            let p0 = ray.loc().clone();
            let num = (AlgVector::from(orig) - AlgVector::from(p0)) * self.norm().clone();
            let scale = num / denom;
            let vec_scale = ray.direction().unary_compose(|e| e.clone() * scale).clone();
            let res_vec = vec_scale + AlgVector::from(ray.loc().clone());
            Some(res_vec.coords().clone())
        }
    }
}

impl<T, const N: usize> Surface<T, N> for Plane<T, N>
where
    T: Default
        + Copy
        + std::ops::Mul<Output = T>
        + std::ops::Add<Output = T>
        + std::ops::Sub<Output = T>
        + std::ops::Div<Output = T>
        + std::cmp::PartialEq,
{
    fn intersect(&self, ray: &Ray<T, N>) -> Vec<Point<T, N>> {
        let ip = self.intersection_point(ray);
        if let Some(p) = ip {
            vec![p]
        } else {
            vec![]
        }
    }
}

#[cfg(test)]
mod test {
    use crate::Normalizable;

    use super::*;

    #[test]
    fn plane_intersection_basic() {
        let p = Plane::new(
            Point::from_components([2.0, 2.0]),
            AlgVector::from(Point::from_components([-1.0, -1.0])).normalize(),
        );
        let r = Ray::new(
            Point::default(),
            AlgVector::from(Point::from_components([0.0, 1.0])).normalize(),
        );
        let ip = p.intersection_point(&r);
        assert_eq!(Some(Point::from_components([0.0, 4.0])), ip)
    }
}
