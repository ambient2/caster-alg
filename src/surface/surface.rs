use crate::{Floatlike, Point, Ray};

pub trait Surface<T, const N: usize> {
    /// Given a ray, returns Some(r) if the ray intersects the surface (w/
    /// the ray corresponding the surface normal ray of the intersection point)
    /// and returns None if the ray doesn't intersect the surface
    fn intersect(&self, ray: &Ray<T, N>) -> Vec<Point<T, N>>;
}

pub struct CompositeSurface<T, const N: usize> {
    constituents: Vec<Box<dyn Surface<T, N>>>,
}

impl<T, const N: usize> CompositeSurface<T, N> {
    pub fn new(constituents: Vec<Box<dyn Surface<T, N>>>) -> Self {
        CompositeSurface { constituents }
    }
}

impl<T, const N: usize> Surface<T, N> for CompositeSurface<T, N>
where
    T: Floatlike,
{
    fn intersect(&self, ray: &Ray<T, N>) -> Vec<Point<T, N>> {
        let mut ps = vec![];
        for c in self.constituents.iter() {
            let res = c.intersect(&ray);
            ps.extend(res);
        }
        ps
    }
}
