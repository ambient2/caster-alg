use crate::{AlgVector, Floatlike, Normalizable, Point, Ray};

use super::Surface;

type IntersectionPoint<T, const N: usize> = Option<(Point<T, N>, Option<T>)>;

#[derive(Debug, PartialEq, Clone)]
pub struct Sphere<T, const N: usize> {
    center: Point<T, N>,
    radius: T,
}

impl<T, const N: usize> Sphere<T, N> {
    pub fn new(center: Point<T, N>, radius: T) -> Sphere<T, N> {
        Sphere { center, radius }
    }
}

impl<T, const N: usize> Sphere<T, N>
where
    T: Floatlike,
{
    pub fn intersection_point_modulo_offset(&self, ray: &Ray<T, N>) -> IntersectionPoint<T, N> {
        let p = ray.loc().clone();
        let d: AlgVector<T, N> = ray.direction().clone();
        let c = self.center.clone();
        let r = self.radius.clone();

        let p_min_c = p - c;
        let p_min_c = AlgVector::from(p_min_c.clone());
        let dir_dot_p_min_c = d.clone() * p_min_c.clone();
        let dir_dot_p_sqrd = dir_dot_p_min_c.clone() * dir_dot_p_min_c.clone();

        let p_min_c_squared = p_min_c.clone() * p_min_c;
        let p_min_c_squared_min_r_squared = p_min_c_squared - r.clone() * r;

        let discr = dir_dot_p_sqrd - p_min_c_squared_min_r_squared;

        if discr < Default::default() {
            return None;
        } else if discr == Default::default() {
            let scaled_dir = d.unary_compose(|f| f.clone() * (-dir_dot_p_min_c.clone()));
            return Some((scaled_dir.coords().clone(), None));
        } else {
            let scaled_dir = d.unary_compose(|f| f.clone() * (-dir_dot_p_min_c.clone()));
            return Some((scaled_dir.coords().clone(), Some(discr)));
        }
    }

    pub fn intesection_points(
        &self,
        ray: &Ray<T, N>,
    ) -> Option<(Point<T, N>, Option<Point<T, N>>)> {
        let ipoint = self.intersection_point_modulo_offset(&ray);
        let res = match ipoint {
            None => None,
            Some((p1, None)) => Some((p1, None)),
            Some((p1, Some(offset))) => {
                let os = ray.direction().normalize().unary_compose(|e| {
                    let sq = offset.psqrt();
                    if sq.is_some() {
                        e.clone() * sq.unwrap()
                    } else {
                        e.clone() * Default::default()
                    }
                });
                let fp1 = p1.clone() - os.coords().clone();
                let fp2 = p1.clone() + os.coords().clone();
                Some((fp1, Some(fp2)))
            }
        };
        res
    }

    pub fn surface_norm_ray(&self, ray: &Ray<T, N>) -> Option<(Ray<T, N>, Option<Ray<T, N>>)> {
        let ipoint = self.intesection_points(&ray);
        match ipoint {
            None => None,
            Some((p1, None)) => Some((
                Ray::new(p1.clone(), AlgVector::from(p1 - self.center.clone())),
                None,
            )),
            Some((p1, Some(p2))) => {
                let l1 = Ray::new(p1.clone(), AlgVector::from(p1 - self.center.clone()));
                let l2 = Ray::new(p2.clone(), AlgVector::from(p2 - self.center.clone()));
                Some((l1, Some(l2)))
            }
        }
    }
}

impl<T, const N: usize> Surface<T, N> for Sphere<T, N>
where
    T: Floatlike,
{
    fn intersect(&self, ray: &Ray<T, N>) -> Vec<Point<T, N>> {
        let snrs = self.surface_norm_ray(&ray);
        match snrs {
            None => vec![],
            Some((r1, None)) => vec![r1.loc().clone()],
            Some((r1, Some(r2))) => vec![r1.loc().clone(), r2.loc().clone()],
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn sphere_calcs_single_intersection_point_correctly() {
        let s = Sphere::new(Point::from_components([1.0, -1.0]), 1.0);
        let r = Ray::new(
            Point::default(),
            AlgVector::from(Point::from_components([1.0, 0.0])),
        );
        let int_point = s.intersection_point_modulo_offset(&r);
        assert_eq!(
            (Point::from_components([1.0, 0.0]), None),
            int_point.unwrap()
        )
    }

    #[test]
    fn sphere_calcs_offset_intersection_point_correctly() {
        let s = Sphere::new(Point::from_components([3.0, 0.0]), 1.0);
        let r = Ray::new(
            Point::default(),
            AlgVector::from(Point::from_components([1.0, 0.0])),
        );
        let int_point = s.intersection_point_modulo_offset(&r);
        assert_eq!(
            (Point::from_components([3.0, 0.0]), Some(1.0)),
            int_point.unwrap()
        )
    }

    #[test]
    fn sphere_calcs_non_intersection_correctly() {
        let s = Sphere::new(Point::from_components([3.0, 3.0]), 1.0);
        let r = Ray::new(
            Point::default(),
            AlgVector::from(Point::from_components([1.0, 0.0])),
        );
        let int_point = s.intersection_point_modulo_offset(&r);
        assert_eq!(None, int_point)
    }

    #[test]
    fn sphere_calcs_float_double_intersection_correctly() {
        let s = Sphere::new(Point::from_components([3.0, 0.0]), 2.0);
        let r = Ray::new(
            Point::default(),
            AlgVector::from(Point::from_components([1.0, 0.0])),
        );
        let int_point = s.intesection_points(&r);
        assert_eq!(
            Some((
                Point::from_components([1.0, 0.0]),
                Some(Point::from_components([5.0, 0.0]))
            )),
            int_point
        )
    }

    #[test]
    fn sphere_calcs_offset_norms_single_correctly() {
        let s = Sphere::new(Point::from_components([1.0, -1.0]), 1.0);
        let r = Ray::new(
            Point::default(),
            AlgVector::from(Point::from_components([1.0, 0.0])),
        );
        let snorm = s.surface_norm_ray(&r);
        assert_eq!(
            Some((
                Ray::new(
                    Point::from_components([1.0, 0.0]),
                    AlgVector::from(Point::from_components([0.0, 1.0]))
                ),
                None
            )),
            snorm
        );
    }

    #[test]
    fn sphere_calcs_offset_norms_correctly() {
        let s = Sphere::new(Point::from_components([1.0, 0.0]), 1.0);
        let r = Ray::new(
            Point::default(),
            AlgVector::from(Point::from_components([1.0, 0.0])),
        );
        let snorm = s.surface_norm_ray(&r);
        assert_eq!(
            Some((
                Ray::new(
                    Point::from_components([0.0, 0.0]),
                    AlgVector::from(Point::from_components([-1.0, 0.0]))
                ),
                Some(Ray::new(
                    Point::from_components([2.0, 0.0]),
                    AlgVector::from(Point::from_components([1.0, 0.0]))
                ))
            )),
            snorm
        );
    }
}
