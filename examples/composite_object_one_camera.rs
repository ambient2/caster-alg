use caster_alg::{AlgVector, CompositeSurface, Normalizable, Point, Ray, Sphere, Surface};
use image::{DynamicImage, GenericImage, Pixel, Rgba};

pub fn main() -> std::io::Result<()> {
    let sphere1 = Sphere::new(Point::from_components([0.0, 0.0, -5.0]), 1.0);
    let sphere2 = Sphere::new(Point::from_components([4.0, 0.0, -10.0]), 1.0);
    let spheres = CompositeSurface::new(vec![Box::from(sphere1), Box::from(sphere2)]);

    let camera = Point::from_components([0.0, 0.0, 0.0]);

    let width_step = 2.0 / 400.0;
    let height_step = 2.0 / 400.0;

    let mut image = DynamicImage::new_rgb8(400, 400);
    let black = Rgba::from_channels(0, 0, 0, 0);
    let white = Rgba::from_channels(0, 255, 0, 0);

    for h in 0..400 {
        for w in 0..400 {
            let x = -1.0 + w as f64 * width_step;
            let y = -1.0 + h as f64 * height_step;
            let z = -1.0;
            let p = Point::from_components([x, y, z]);
            let v = AlgVector::from(p).normalize();

            let r = Ray::new(camera.clone(), v.clone());

            let res = spheres.intersect(&r);
            if res.len() != 0 {
                image.put_pixel(w, h, white);
            } else {
                image.put_pixel(w, h, black);
            }
        }
    }

    image
        .save("examples/composite_object_one_camera.rs.png")
        .expect("Error saving image");
    Ok(())
}
